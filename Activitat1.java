import java.util.Scanner;

public class Activitat1 {

    static  public void main(String[]args){

        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce puntuacion");

        int num = tec.nextInt();
        if( num >= 0 && num < 3 ){
            System.out.println("Muy deficiente.");
        }else if(num >= 3 && num < 5 ){
            System.out.println("Insuficiente.");
        }else if(num >= 5 && num < 6 ){
            System.out.println("Suficiente.");
        }else if(num >= 6 && num < 7 ){
            System.out.println("Bien.");
        }else if(num >= 7 && num < 9 ){
            System.out.println("Notable.");
        }else if(num >= 9 && num <= 10 ){
            System.out.println("Sobresasilente");
        }else {
            System.out.println("Fuera de rango...");
        }

    }
}
