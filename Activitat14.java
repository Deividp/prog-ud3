import java.util.Scanner;

public class Activitat14 {

    public static void main(String[]args){

        //Activitat14.- Escriu un programa que ens permeta determinar si utilitzar els números aleatoris de la classe Math són apropiats.
        // El programa ha de simular que es llança una moneda un número elevat de vegades, per exemple, 1.000.000. A continuació ha
        // d'imprimir per pantalla el número de cares i el número de creus que han eixit. També s'ha de mostrar el percentatge que
        // representa cada valor.
        //Ací tens un exemple:
        //Número de cares: 500627 (50.0627%)
        //Número de creus: 499373 (49.9373%)

        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce el numero de veces que quieres que se lance la moneda:");
        float veces = tec.nextInt();
        int cara = 0;
        int cruz = 0;
        float porCienCara = 0;
        float porCienCruz= 0;

        for (int i = 0; i < veces; i++){

            int moneda = (int) (Math.random()*10);
            if (moneda < 5){
                cara++;
            }else{
                cruz++;
            }
        }
        porCienCara = (cara * 100/veces);
        porCienCruz = (cruz * 100/veces);
        System.out.println("Veces ha salido cara, " + cara + "(" + porCienCara + "%).");
        System.out.println("Veces ha salido cruz," + cruz + "(" + porCienCruz + "%).");
    }
}
