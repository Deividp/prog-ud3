public class Activitat4 {


    public static void main (String[] args) {
        int x = 1;
        System.out.println("Abans del bucle");
        while (x < 4) {
            System.out.println("En el bucle");
            System.out.println("El valor d'x és " + x);
            x = x + 1;
        }
        System.out.println("Després del bucle");
    }
}
