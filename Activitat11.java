import java.util.Scanner;

public class Activitat11 {

    //Escriu un programa que calcula la potència d'un número real (a) elevat a un número enter (b).
// Has de tindre en compte que tant a com b poden valer 0 o poden ser números negatius.
// Has d’implementar la funcionalitat, no emprar el mètode de Java.
    public static void main(String[] args) {

        int resultado = 0;
        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce la base: ");
        int base = tec.nextInt();
        System.out.println("Introduce la potencia");
        int potencia = tec.nextInt();

        if (potencia == 0) {
            base = 1;
        } else if (potencia > 0) {

            for (int i = 1; i < potencia; i++) {

                resultado = base * base;

            }

        } else {
            for (int j = -1; j > potencia; j++) {
                resultado = base / base;

            }



        }
        System.out.println("El valor es: " + resultado + ".");
    }
}