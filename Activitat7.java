public class Activitat7 {

    //Mostrar els números imparells d'1 a 99 indicant al final quants en són.
    // Repetir per als parells i per als múltiples de cinc.

    public static void main (String[]args){


        for (int i = 1; i<=99 ; i+=2){

            System.out.print(" " + i + ",");

        }
    }
}
