public class Activitat10 {

    //Activitat 10.- Visualitzar les taules de multiplicar de l'1 al 10.

    public static void main(String[]args){

        for (int i = 1; i<11;i++){
            System.out.println();
            for (int j = 0; j<11;j++){

                System.out.println(" " + i +" x " + j +" = " + (i * j));

            }
        }

    }
}
