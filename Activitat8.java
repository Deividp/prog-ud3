import java.util.Scanner;

public class Activitat8 {

    // Introduir per teclat dos números, A i B (A major que B).
    // Visualitzar els números des d'A fins a B i indicar quants n'hi ha que siguen parells.

    public static void main (String[]args){

        Scanner tec = new Scanner (System.in);
        System.out.println("Introduce un numero:");
        int aNum = tec.nextInt();
        System.out.println("Introduce otro numero: ");
        int bNum = tec.nextInt();
        System.out.print(aNum);
        while (aNum < bNum){
            aNum++;
            System.out.print(" " + aNum + ",");
        }
        System.out.println(" estas fuera del bucle.");
    }
}
