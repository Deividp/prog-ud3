public class Activitat6 {

    //Calcular la suma dels números compresos entre 1 i 1000.

    public static void main(String[]args){

        int sumador = 0;

        for (int i = 1; i < 1000; i++){

            sumador += i;
        }
        System.out.println("La suma total es " + sumador);
    }
}
