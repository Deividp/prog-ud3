import java.util.Scanner;

public class Activitat2 {

    static  public void main(String[]args){

        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce la cantidad de dinero: ");
        double auxiliar = tec.nextDouble();
        int centimos = (int) (auxiliar * 100);
            centimos = centimos % 100;
        int numDinero = (int) auxiliar;

        /* Billetes */

        if(numDinero>=500){
            int quinientos = (int) numDinero / 500;
            System.out.println( quinientos + " billetes de 500 ");
            numDinero = numDinero % 500;
        }
        if( numDinero >= 200){
            int dosCientos = (int) numDinero / 200;
            System.out.println( dosCientos + " billetes de 200 ");
            numDinero = numDinero % 200;
        }
        if( numDinero >= 100){
            System.out.println("1 billetes de 100 ");
            numDinero = numDinero % 100;
        }
        if( numDinero >= 50){
            System.out.println("1 billetes de 50 ");
            numDinero = numDinero % 50;
        }
        if( numDinero >= 20){
            int venteEurs = (int) numDinero / 20;
            System.out.println( venteEurs + " billetes de 20 ");
            numDinero = numDinero % 20;
        }

        if( numDinero >= 10){
            System.out.println("1 billetes de 10 ");
            numDinero = numDinero % 10;
        }
        if( numDinero >= 5){
            System.out.println("1 billetes de 5 ");
            numDinero = numDinero % 5;
        }
        /* Monedas euro */

        if( numDinero >= 2) {
            int moneda2Euros = (int) numDinero /2;
            System.out.println(moneda2Euros + " monedas de dos € ");
            numDinero = numDinero % 2;
        }
        if( numDinero >= 1) {
            int moneda2Euros = (int) numDinero / 1;
            System.out.println(moneda2Euros + " 1 monedas de un € ");
            numDinero = numDinero - 1;
        }
        /* monedas de centimos */
        System.out.println(centimos);
        if(centimos >= 50){
            System.out.println("1 moneda de 50 centimos ");
            centimos = centimos % 50;
        }
        if(centimos >= 20){
            int centimos20 = centimos / 20;
            System.out.println(centimos20 + " monedas de 20 centimos");
            centimos = centimos % 20;
        }
        if(centimos >= 10){
            System.out.println("1 monedas de 10 centimos");
            centimos = centimos % 10;
        }
        if(centimos >= 5){
            System.out.println("1 monedas de 5 centimos");
        }
        if(centimos >= 2){
            int centimos2 = centimos / 2;
            System.out.println(centimos2 + " monedas de 2 centimos");
            centimos = centimos % 2;
        }
        if(centimos == 1){
            System.out.println("una moneda de 1 centimo");
        }
    }
}
