public class Activitat9 {

    public static void main(String[]args){

        //Activitat9.- Visualitzar els números d'1 a 100 i indicar al final:
        //    • Quants números parells n'hi ha i la seua suma.
        //    • Quants números imparells n'hi ha i la seua suma.
        //    • Quants n'hi ha que siguen múltiples de quatre i de set i la seua suma
        int suma = 0;
        int part = 0;
        int impart = 0;
        int multSiete = 0;
        int multCuatro = 0;
        for (int i = 1; i < 100;i++){
            suma+=i;

            if(i%2==0){
                part++;
            }else{
                impart++;
            }
            if(i%7==0){
                multSiete++;
            }
            if (i%4==0){
                multCuatro++;
            }
        }
        System.out.println("Suma = " + suma + ", pares = " + part + ", impares = " + impart + ", multiples de 7 = " + multSiete + ", multiples de 4 = " + multCuatro +".");
    }
}
